Este plugin genera páginas automáticamente a partir de un directorio lleno de imágenes. Esta versión genera, además, páginas con el campo "date" a partir de los nombres de los archivos (siempre y cuando esta fecha esté en el formato YYYYMMDD. Esto puede ser ajustado en el archivo img_page_generator.rb en caso de ser necesario).

# Instrucciones:

1. Añade los archivos del repositorio a tu proyecto de Jekyll.
2. En tu `config.yml`, agrega las siguientes líneas y ajústalas a tu antojo:

```
# imagPageGen plugin settings
images_folder: "/genImag" # relative path of the folder where the images are stored
images_collection: "col1" # name of the collection
images_ordinal_title: "Imag #" # title given to each page taking into account its order
images_filename_template: "Filename_" # the characters in your filenames preceding the date in format YYYYMMDD
```

3. Agrega imágenes al directorio que configuraste previamente. Las páginas generadas estarán ubicadas bajo la subdirección `/coleccion/nombreDeArchivo/`. __Nota cómo en las imágenes incluidas como prueba en el plugin, sólo las páginas generadas a partir de los archivos que comienzan con `"Filename_"` incluyen su fecha.__

4. En caso de querer comprobar fácilmente la publicación de las imágenes, agrega las siguientes líneas a tu index.html:

```
<div class="posts">

      {% for col in site.collections %}
        {%- if col.label == site.images_collection -%}
          {% for post in col.docs %}
            <a class="posts__link" href="{{ post.url | prepend: site.baseurl }}" >
                <div>
                    <h2><strong>{{ post.title }}</strong> </h2>
                </div>
            </a>            
          {% endfor %}
        {%- endif -%}

      {% endfor %}
</div>
```
